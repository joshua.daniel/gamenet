using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [Header("Login Panel")]
    public GameObject LoginPanel;
    public InputField PlayerNameInput;

    [Header("Connecting Panel")]
    public GameObject ConnectingPanel;

    [Header("Join Room Panel")]
    public GameObject JoinRoomPanel;
    public TMP_InputField RoomCodeInput;
    public TextMeshProUGUI JoinFailedText;

    [Header("Inside Room Panel")]
    public GameObject InsideRoomPanel;
    public TextMeshProUGUI RoomCodeText;
    public TextMeshProUGUI Player1NameText;
    public TextMeshProUGUI Player2NameText;
    public Button StartButton;

    // Start is called before the first frame update
    void Start()
    {
        ActivatePanel(LoginPanel.name);

        PhotonNetwork.AutomaticallySyncScene = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region UI Callbacks
    public void OnLoginButtonClicked()
    {
        string playerName = PlayerNameInput.text;

        if (!string.IsNullOrEmpty(playerName))
        {
            ActivatePanel(ConnectingPanel.name);

            if (!PhotonNetwork.IsConnected)
            {
                PhotonNetwork.LocalPlayer.NickName = playerName;
                PhotonNetwork.ConnectUsingSettings();
            }
        }
    }

    public void OnCreateRoomButtonClicked()
    {
        ActivatePanel(ConnectingPanel.name);

        string roomName = GenerateRoomCode(RoomCodeInput.characterLimit);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 2;

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }

    public void OnJoinRoomButtonClicked()
    {
        if (RoomCodeInput.text.Length < RoomCodeInput.characterLimit)
        {
            if (JoinFailedTextCoroutine != null)
            {
                StopCoroutine(JoinFailedTextCoroutine);
            }

            JoinFailedTextCoroutine = StartCoroutine(ShowJoinFailedText("Not enough characters..."));

            return;
        }

        PhotonNetwork.JoinRoom(RoomCodeInput.text.ToUpper());

        ActivatePanel(ConnectingPanel.name);
    }

    public void OnJoinRandomRoomButtonClicked()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    public void OnCancelButtonClicked()
    {
        PhotonNetwork.LeaveRoom();
    }

    public void OnStartButtonClicked()
    {
        //PhotonNetwork.LocalPlayer.SetCustomProperties(playerRoleMole);
        //PhotonNetwork.PlayerList[1].SetCustomProperties(playerRoleBatter);

        PhotonNetwork.LoadLevel("GameScene");
    }

    #endregion

    #region PUN Callbacks
    public override void OnConnectedToMaster()
    {
        ActivatePanel(JoinRoomPanel.name);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        OnCreateRoomButtonClicked();
    }

    public override void OnJoinedRoom()
    {
        ActivatePanel(InsideRoomPanel.name);

        RoomCodeText.text = PhotonNetwork.CurrentRoom.Name;

        if (PhotonNetwork.IsMasterClient)
        {
            Player1NameText.text = PhotonNetwork.LocalPlayer.NickName;
            Player2NameText.text = "";
        }
        else
        {
            Player1NameText.text = PhotonNetwork.MasterClient.NickName;
            Player2NameText.text = PhotonNetwork.LocalPlayer.NickName;
        }

        StartButton.gameObject.SetActive(PhotonNetwork.IsMasterClient);
        StartButton.GetComponent<Button>().interactable = false;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Player2NameText.text = newPlayer.NickName;

        if (PhotonNetwork.IsMasterClient)
        {
            StartButton.GetComponent<Button>().interactable = PhotonNetwork.PlayerList.Length == PhotonNetwork.CurrentRoom.MaxPlayers;
        }

        ExitGames.Client.Photon.Hashtable playerRoleMole = new ExitGames.Client.Photon.Hashtable()
        {
            { Constants.PLAYER_ROLE, Constants.ROLE_MOLE }
        };

        ExitGames.Client.Photon.Hashtable playerRoleBatter = new ExitGames.Client.Photon.Hashtable()
        {
            { Constants.PLAYER_ROLE, Constants.ROLE_BATTER }
        };

        List<ExitGames.Client.Photon.Hashtable> playerRoles = new List<ExitGames.Client.Photon.Hashtable>()
        {
            playerRoleMole, playerRoleBatter
        };

        int selection = Random.Range(0, playerRoles.Count);

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            selection = Random.Range(0, playerRoles.Count);
            if (selection > -1)
            {
                player.SetCustomProperties(playerRoles[selection]);
                playerRoles.RemoveAt(selection);
            }
        }

        //foreach (Player player in PhotonNetwork.PlayerList)
        //{
        //    Debug.Log(player.NickName + " " + PhotonNetwork.PlayerList.);
        //}

        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            Debug.Log(PhotonNetwork.PlayerList[i].NickName + " " + i);
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        if (JoinFailedTextCoroutine != null)
        {
            StopCoroutine(JoinFailedTextCoroutine);
        }
        
        JoinFailedTextCoroutine = StartCoroutine(ShowJoinFailedText(message));
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        if (JoinFailedTextCoroutine != null)
        {
            StopCoroutine(JoinFailedTextCoroutine);
        }

        JoinFailedTextCoroutine = StartCoroutine(ShowJoinFailedText(message));
    }

    public override void OnLeftRoom()
    {
        ActivatePanel(JoinRoomPanel.name);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Player1NameText.text = PhotonNetwork.LocalPlayer.NickName;
        Player2NameText.text = "";

        if (PhotonNetwork.IsMasterClient)
        {
            StartButton.GetComponent<Button>().interactable = PhotonNetwork.PlayerList.Length == PhotonNetwork.CurrentRoom.MaxPlayers;
        }
    }

    #endregion

    #region Public Methods
    public void ActivatePanel(string panelName)
    {
        LoginPanel.SetActive(LoginPanel.name.Equals(panelName));
        ConnectingPanel.SetActive(ConnectingPanel.name.Equals(panelName));
        JoinRoomPanel.SetActive(JoinRoomPanel.name.Equals(panelName));
        InsideRoomPanel.SetActive(InsideRoomPanel.name.Equals(panelName));
    }

    #endregion

    #region Private Methods
    Coroutine JoinFailedTextCoroutine;

    IEnumerator ShowJoinFailedText(string text)
    {
        JoinFailedText.gameObject.SetActive(true);
        JoinFailedText.text = text;

        yield return new WaitForSeconds(3.0f);

        JoinFailedText.gameObject.SetActive(false);
    }

    string GenerateRoomCode(int roomCodeLength)
    {
        string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string roomName = "";

        for (int i = 0; i < roomCodeLength; i++)
        {
            roomName += chars[Random.Range(0, chars.Length)];
        }

        return roomName;
    }

    #endregion
}
