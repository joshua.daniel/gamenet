
public class Constants
{
    public const string PLAYER_ROLE = "playerRole";

    public const string ROLE_MOLE = "mole";
    public const string ROLE_BATTER = "batter";

    public const byte MoleScoredCode = 1;
    public const byte BatterScoredCode = 2;
    public const byte UpdateTimerCode = 3;
    public const byte GameEndCode = 4;
}
