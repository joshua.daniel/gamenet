using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MoleMovement : MonoBehaviour
{
    [Header("References")]
    public Transform CameraParent;
    public Transform CameraBoom;

    private CharacterController Controller;

    [Header("Physics")]
    public float speed;
    public float turnSpeed;
    public float jumpForce;
    public float cameraSensitivity;
    public float gravity;

    private Vector3 direction;
    private Vector3 jumpVelocity;

    private Vector2 mouseInput;
    private float cameraPitch;
    private float cameraYaw;

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerRole;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_ROLE, out playerRole))
            {
                if ((string)playerRole != Constants.ROLE_MOLE)
                {
                    CameraParent.gameObject.SetActive(false);
                    this.enabled = false;
                }
            }
        }

        Controller = GetComponent<CharacterController>();

        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        direction = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        direction = CameraParent.TransformDirection(direction);

        if (Controller.isGrounded)
        {
            jumpVelocity.y = -1f;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                jumpVelocity.y = jumpForce;
            }
        }
        else
        {
            jumpVelocity.y -= gravity * -2f * Time.deltaTime;
        }

        mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        cameraPitch -= mouseInput.y * cameraSensitivity;
        cameraYaw += mouseInput.x * cameraSensitivity;
    }

    private Vector3 camRot;

    private void LateUpdate()
    {
        Controller.Move(direction * speed * Time.deltaTime);
        Controller.Move(jumpVelocity * Time.deltaTime);

        if (direction != Vector3.zero)
        {
            Quaternion lookRotation = Quaternion.LookRotation(direction, Vector3.up);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, lookRotation, turnSpeed * Time.deltaTime);
        }

        //CameraParent.Rotate(0f, mouseInput.x * cameraSensitivity, 0f);
        CameraParent.rotation = Quaternion.Euler(0f, cameraYaw, 0f);
        CameraBoom.localRotation = Quaternion.Euler(cameraPitch, 0f, 0f);
    }
}
