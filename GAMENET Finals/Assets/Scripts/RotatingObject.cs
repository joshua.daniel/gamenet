using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingObject : MonoBehaviour
{
    public float rotationSpeed;
    public Vector3 eulerAngles;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void LateUpdate()
    {
        transform.Rotate(eulerAngles * rotationSpeed * Time.deltaTime);
    }
}
