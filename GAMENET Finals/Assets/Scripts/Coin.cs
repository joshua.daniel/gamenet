using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Coin : MonoBehaviourPunCallbacks
{
    public int pointYield;

    public CoinNode node;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Mole" && !GameManager.instance.gameOver)
        {
            if (photonView.IsMine)
            {
                if (node != null) node.placedCoin = null;
                photonView.RPC("CreditCoin", RpcTarget.All);
            }
        }
    }

    [PunRPC]
    public void CreditCoin()
    {
        object data = pointYield;

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.RaiseEvent(Constants.MoleScoredCode, data, raiseEventOptions, sendOption);

            CoinSpawner.instance.currentCoins--;
        }

        Destroy(this.gameObject);
    }
}
