using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CoinSpawner : MonoBehaviour
{
    public static CoinSpawner instance;

    public Transform CoinSpawnerParent;

    public int maxCoins;
    public float spawnDelay;

    private CoinNode[] Nodes;

    [HideInInspector] public int currentCoins;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            this.enabled = false;
            return;
        }

        Nodes = new CoinNode[CoinSpawnerParent.childCount];

        for (int i = 0; i < Nodes.Length; i++)
        {
            Nodes[i] = CoinSpawnerParent.GetChild(i).GetComponent<CoinNode>();
        }

        // Spawn Initial Coins
        while (currentCoins < maxCoins)
        {
            SpawnRandomCoin();
        }

        StartCoroutine(SpawnCoins());
    }

    IEnumerator SpawnCoins()
    {
        while (true)
        {
            if (currentCoins < maxCoins)
            {
                SpawnRandomCoin();
            }

            yield return new WaitForSeconds(spawnDelay);
        }
    }

    void SpawnRandomCoin()
    {
        int index = Random.Range(0, Nodes.Length);

        while (Nodes[index].placedCoin != null)
        {
            index = Random.Range(0, Nodes.Length);
        }

        Nodes[index].SpawnCoin();
    }
}
