using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class MoleCollision : MonoBehaviour
{
    public GameObject Body;
    private MoleMovement Movement;

    public float respawnDelay;
    public int pointYield;
    public int pointsLost;

    // Start is called before the first frame update
    void Start()
    {
        Movement = GetComponent<MoleMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Bat" && Body.activeSelf && !GameManager.instance.gameOver)
        {
            StartCoroutine(BeginRespawning());
        }
    }

    IEnumerator BeginRespawning()
    {
        Body.SetActive(false);
        Movement.enabled = false;

        object[] data = new object[] { pointYield, pointsLost };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        if (PhotonNetwork.IsMasterClient) PhotonNetwork.RaiseEvent(Constants.BatterScoredCode, data, raiseEventOptions, sendOption);

        yield return new WaitForSeconds(respawnDelay);

        object playerRole;
        if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_ROLE, out playerRole))
        {
            if ((string)playerRole == Constants.ROLE_MOLE)
            {
                GameManager.instance.RespawnMole();
            }
        }

        Destroy(this.gameObject);
    }
}
