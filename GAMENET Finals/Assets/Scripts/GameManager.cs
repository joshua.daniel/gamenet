using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance;

    [Header("Quit Panel")]
    public GameObject QuitPanel;

    [Header("Game Over Panel")]
    public GameObject GameOverPanel;
    public TextMeshProUGUI WinnerText;
    public TextMeshProUGUI WinningPlayerText;

    [Header("References")]
    public Transform MoleSpawnpoint;
    public Transform BatterSpawnpoint;
    public GameObject BatterCamera;

    public GameObject MolePrefab;
    public GameObject BatterPrefab;

    [HideInInspector] public bool gameOver = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance == this)
        {
            Destroy(this.gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerRole;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_ROLE, out playerRole))
            {
                if ((string)playerRole == Constants.ROLE_MOLE)
                {
                    BatterCamera.SetActive(false);
                    PhotonNetwork.Instantiate(MolePrefab.name, MoleSpawnpoint.position, MoleSpawnpoint.rotation);
                }
                else if ((string)playerRole == Constants.ROLE_BATTER)
                {
                    BatterCamera.SetActive(true);
                    PhotonNetwork.Instantiate(BatterPrefab.name, BatterSpawnpoint.position, BatterSpawnpoint.rotation);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitPanel.SetActive(!QuitPanel.activeSelf);
            Cursor.visible = !Cursor.visible;
        }
    }

    public void OnQuitButtonClicked()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        Cursor.visible = true;
        PhotonNetwork.LoadLevel("LobbyScene");
    }

    public void RespawnMole()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " is spawning mole");
        PhotonNetwork.Instantiate(MolePrefab.name, MoleSpawnpoint.position, MoleSpawnpoint.rotation);
    }

    public void DeclareWinner(string winner)
    {
        gameOver = true;

        GameOverPanel.SetActive(true);

        WinnerText.text = "The " + winner.ToUpper() + " Wins!";

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            object playerRole;
            if (player.CustomProperties.TryGetValue(Constants.PLAYER_ROLE, out playerRole))
            {
                if ((string)playerRole == winner)
                {
                    WinningPlayerText.text = player.NickName;

                    if (player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
                        WinningPlayerText.color = Color.yellow;
                }
            }
        }
    }
}
