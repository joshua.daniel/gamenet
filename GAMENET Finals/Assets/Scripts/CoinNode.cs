using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class CoinNode : MonoBehaviour
{
    public GameObject coinPrefab;

    public Coin placedCoin;

    public void SpawnCoin()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            CoinSpawner.instance.currentCoins++;
        }

        Coin spawnedCoin = PhotonNetwork.Instantiate(coinPrefab.name, transform.position, transform.rotation).GetComponent<Coin>();
        spawnedCoin.node = this;
        placedCoin = spawnedCoin;
    }
}
