using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BatterController : MonoBehaviourPunCallbacks
{
    public float swingSpeed;
    public float aimSpeed;

    private float yaw;
    private float yRot;

    private Animator animator;

    private bool controlEnabled = true;

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerRole;
            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_ROLE, out playerRole))
            {
                if ((string)playerRole != Constants.ROLE_BATTER)
                {
                    controlEnabled = false;
                }
            }
        }

        Cursor.visible = false;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (controlEnabled)
        {
            if (animator.GetBool("isSwinging"))
            {
                yaw = 0;
            }
            else
            {
                yaw = Input.GetAxis("Horizontal");
            }

            if (transform.localEulerAngles.y > 180)
            {
                yRot = Mathf.Max(290, transform.localEulerAngles.y);
            }
            else
            {
                yRot = Mathf.Min(70, transform.localEulerAngles.y);
            }
        }        

        if (controlEnabled && Input.GetKeyDown(KeyCode.Space) && !animator.GetBool("isSwinging"))
        {
            photonView.RPC("ToggleSwinging", RpcTarget.All);
        }
    }

    private void LateUpdate()
    {
        if (controlEnabled)
        {
            transform.localEulerAngles = new Vector3(transform.eulerAngles.x,
                                                yRot,
                                                transform.eulerAngles.z);

            transform.Rotate(0f, yaw * aimSpeed * Time.deltaTime, 0f);
        }
    }

    [PunRPC]
    public void ToggleSwinging()
    {
        animator.SetBool("isSwinging", !animator.GetBool("isSwinging"));
    }
}
