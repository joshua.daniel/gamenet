using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class ScoreKeeper : MonoBehaviourPunCallbacks
{
    public TextMeshProUGUI MoleScoreText;
    public TextMeshProUGUI BatterScoreText;
    public TextMeshProUGUI TimerText;

    public float timer;

    private int moleScore;
    private int batterScore;

    private void Start()
    {
        MoleScoreText.text = moleScore.ToString();
        BatterScoreText.text = batterScore.ToString();
    }

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient && !GameManager.instance.gameOver)
        {
            float oldTime = Mathf.Ceil(timer);
            timer -= Time.deltaTime;
            timer = Mathf.Max(0, timer);

            if (oldTime - timer >= 1f && PhotonNetwork.IsMasterClient)
            {
                RaiseEventOptions raiseEventOptions = new RaiseEventOptions
                {
                    Receivers = ReceiverGroup.All,
                    CachingOption = EventCaching.AddToRoomCache
                };

                SendOptions sendOption = new SendOptions
                {
                    Reliability = false
                };

                object data = timer;

                PhotonNetwork.RaiseEvent(Constants.UpdateTimerCode, data, raiseEventOptions, sendOption);
            }
        }
    }

    private void OnEnable()
    {
        base.OnEnable();

        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        base.OnDisable();

        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == Constants.MoleScoredCode)
        {
            moleScore += (int)photonEvent.CustomData;

            MoleScoreText.text = moleScore.ToString();
        }

        if (photonEvent.Code == Constants.BatterScoredCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            batterScore += (int)data[0];
            moleScore -= (int)data[1];

            moleScore = Mathf.Max(0, moleScore);

            MoleScoreText.text = moleScore.ToString();
            BatterScoreText.text = batterScore.ToString();
        }

        if (photonEvent.Code == Constants.UpdateTimerCode)
        {
            float timeRemaining = (float)photonEvent.CustomData;
            TimerText.text = Mathf.Round(timeRemaining).ToString();

            if (timeRemaining <= 0)
            {
                string winner = moleScore > batterScore ? Constants.ROLE_MOLE : Constants.ROLE_BATTER;

                GameManager.instance.DeclareWinner(winner);
            }
        }
    }
}
