using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class ShooterUI : MonoBehaviour
{
    public GameObject killNotifPrefab;

    public GameObject killFeedContent;
    public GameObject fireButton;
    public GameObject joystick;

    public GameObject gameEndPanel;
    public Text winnerText;

    public int maxKillNotifs;

    public void CreateKillNotif(PhotonMessageInfo info)
    {
        if (killFeedContent.transform.childCount >= maxKillNotifs) Destroy(killFeedContent.transform.GetChild(0).gameObject);

        var killNotif = Instantiate(killNotifPrefab).GetComponent<KillNotification>();
        killNotif.transform.SetParent(killFeedContent.transform);
        killNotif.transform.localScale = Vector3.one;

        killNotif.killerText.text = info.Sender.NickName + " killed";
        killNotif.killedText.text = info.photonView.Owner.NickName;

        if (info.Sender.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            killNotif.youAreTheKiller.SetActive(true);
        }
        else if (info.photonView.Owner.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            killNotif.youAreKilled.SetActive(true);
        }

        Destroy(killNotif.gameObject, 5.0f);
    }

    public void SetActiveControlsUI(bool value)
    {
        fireButton.SetActive(value);
        joystick.SetActive(value);
    }
}
