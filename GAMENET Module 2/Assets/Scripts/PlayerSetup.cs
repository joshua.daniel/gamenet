using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject fpsModel;
    public GameObject nonFpsModel;

    public GameObject playerUiPrefab;

    public TextMeshProUGUI playerNameText;

    public PlayerMovementController _playerMovementController;
    public Camera fpsCamera;
    private AudioListener listener;

    private Animator _animator;
    public Avatar fpsAvatar, nonFpsAvatar;

    private Shooting _shooting;

    // Start is called before the first frame update
    void Start()
    {
        _playerMovementController = this.GetComponent<PlayerMovementController>();
        _animator = this.GetComponent<Animator>();
        listener = fpsCamera.GetComponent<AudioListener>();

        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);
        _animator.SetBool("isLocalPlayer", photonView.IsMine);
        _animator.avatar = photonView.IsMine ? fpsAvatar : nonFpsAvatar;

        _shooting = this.GetComponent<Shooting>();

        playerNameText.text = photonView.Owner.NickName;

        if (photonView.IsMine)
        {
            GameObject _playerUi = Instantiate(playerUiPrefab);
            _playerMovementController.fixedTouchField = _playerUi.transform.Find("RotationTouchField").GetComponent<FixedTouchField>();
            _playerMovementController.joystick = _playerUi.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            fpsCamera.enabled = true;
            listener.enabled = true;

            _playerUi.transform.Find("FireButton").GetComponent<Button>().onClick.AddListener(() => _shooting.Fire());
        }
        else
        {
            _playerMovementController.enabled = false;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            fpsCamera.enabled = false;
            listener.enabled = false;
        }
    }
}
