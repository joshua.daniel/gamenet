using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance;

    public GameObject playerPrefab;

    [SerializeField] private int winningKills = 10;
    [SerializeField] private float returnLobbyDelay = 5f;

    private bool winnerDeclared;

    public int WinningKills => winningKills;

    private void Awake()
    {
        if (GameManager.instance != null) Destroy(this.gameObject);
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            PhotonNetwork.Instantiate(playerPrefab.name, SpawnpointManager.instance.GetRandomSpawnpoint(), Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PhotonNetwork.LeaveRoom();
        }
    }

    public override void OnLeftRoom()
    {
        PhotonNetwork.LoadLevel("LobbyScene");
    }

    public void DeclareWinner(PhotonView winningPlayer)
    {
        var playerUI = GameObject.FindWithTag("PlayerUI").GetComponent<ShooterUI>();
        
        if (playerUI.gameEndPanel.activeSelf) return;

        playerUI.gameEndPanel.SetActive(true);

        playerUI.winnerText.text = winningPlayer.IsMine ? "YOU WIN!" : "WINNER: " + winningPlayer.Owner.NickName;
        playerUI.SetActiveControlsUI(false);

        var players = GameObject.FindGameObjectsWithTag("Player");

        foreach (var player in players)
        {
            player.GetComponent<PlayerMovementController>().enabled = false;
        }

        if (PhotonNetwork.IsMasterClient) StartCoroutine(ReturnAllToLobby());
    }

    IEnumerator ReturnAllToLobby()
    {
        yield return new WaitForSeconds(returnLobbyDelay);

        if (PhotonNetwork.IsMasterClient) PhotonNetwork.LoadLevel("LobbyScene");
    }
}
