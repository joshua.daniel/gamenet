using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera _camera;
    public GameObject hitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    private Animator _animator;

    private Text killsText;
    private int playerKills;

    private bool isDead;

    public bool IsDead => isDead;

    // Start is called before the first frame update
    void Start()
    {
        RegainHealth();
        _animator = this.GetComponent<Animator>();

        if (photonView.IsMine)
        {
            killsText = GameObject.Find("KillCountText").GetComponent<Text>();
            killsText.text = playerKills.ToString();
        }
    }

    public void Fire()
    {
        if (isDead) return;

        RaycastHit hit;
        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            GameObject hitObject = hit.collider.gameObject;

            if (hitObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine &&
                !hitObject.GetComponent<Shooting>().IsDead)
            {
                hitObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25, photonView.ViewID);
            }
        }
    }
    
    [PunRPC]
    public void TakeDamage(int damage, int instigatorID, PhotonMessageInfo info)
    {
        if (_animator.GetBool("isDead")) return;

        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();
            Debug.Log(info.Sender.NickName + " kiled " + info.photonView.Owner.NickName);

            PhotonView.Find(instigatorID).GetComponent<Shooting>().AddKill();

            GameObject.FindWithTag("PlayerUI").GetComponent<ShooterUI>().CreateKillNotif(info);
        }
    }

    public void AddKill()
    {
        playerKills++;

        if (photonView.IsMine) killsText.text = playerKills.ToString();

        if (playerKills >= GameManager.instance.WinningKills)
        {
            GameManager.instance.DeclareWinner(this.photonView);
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        isDead = true;

        if (photonView.IsMine)
        {
            _animator.SetBool("isDead", true);
            this.GetComponent<PlayerMovementController>().enabled = false;
            GameObject.FindWithTag("PlayerUI").GetComponent<ShooterUI>().SetActiveControlsUI(false);

            if (playerKills >= GameManager.instance.WinningKills) return;

            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
            
            yield return new WaitForSeconds(1.0f);
            respawnTime--;
        }

        _animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        this.transform.position = SpawnpointManager.instance.GetRandomSpawnpoint();
        this.GetComponent<PlayerMovementController>().enabled = true;
        GameObject.FindWithTag("PlayerUI").GetComponent<ShooterUI>().SetActiveControlsUI(true);

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;

        isDead = false;
    }
}
