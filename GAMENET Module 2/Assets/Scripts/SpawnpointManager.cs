using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnpointManager : MonoBehaviour
{
    public static SpawnpointManager instance;

    private Transform[] spawnpoints;

    private void Awake()
    {
        if (SpawnpointManager.instance != null) Destroy(this.gameObject);
        instance = this;

        spawnpoints = new Transform[transform.childCount];

        for (int i = 0; i < spawnpoints.Length; i++)
        {
            spawnpoints[i] = transform.GetChild(i);
        }
    }

    public Vector3 GetRandomSpawnpoint()
    {
        return spawnpoints[Random.Range(0, spawnpoints.Length)].position;
    }
}
