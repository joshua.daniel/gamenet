using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillNotification : MonoBehaviour
{
    public Text killerText;
    public Text killedText;
    public GameObject youAreTheKiller;
    public GameObject youAreKilled;
}
