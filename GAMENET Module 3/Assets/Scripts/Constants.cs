

public class Constants
{
    public const string PLAYER_READY = "isPlayerReady";
    public const string PLAYER_SELECTION_NUMBER = "playerSelectionNumber";

    public const byte PlayerDeathCode = 1;
    public const byte PlayerWonCode = 2;
}
