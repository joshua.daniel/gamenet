using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using ExitGames.Client.Photon;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DeathRaceGameManager : MonoBehaviourPunCallbacks
{
    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;

    public static DeathRaceGameManager instance = null;

    public Text timeText;
    public Text killFeedText;
    public GameObject winnerPanel;
    public Text winnerText;
    public GameObject escapePanel;

    public List<GameObject> players = new List<GameObject>();

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == Constants.PlayerDeathCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            int killedActor = (int)data[0];
            string killedName = (string)data[1];
            int killerActor = (int)data[2];
            string killerName = (string)data[3];

            StartCoroutine(DisplayKillFeed(killedActor, killedName, killerActor, killerName));
        }

        if (photonEvent.Code == Constants.PlayerWonCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            int winnerActor = (int)data[0];
            string winnerName = (string)data[1];

            winnerPanel.SetActive(true);

            if (winnerActor == PhotonNetwork.LocalPlayer.ActorNumber) winnerText.text = "YOU WIN!!!";
            else winnerText.text = "WINNER: " + winnerName;
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                Debug.Log("Spawning");
                PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escapePanel.SetActive(!escapePanel.activeSelf);
        }
    }

    IEnumerator DisplayKillFeed(int killedActor, string killedName, int killerActor, string killerName)
    {
        killFeedText.gameObject.SetActive(true);

        killFeedText.text = killerName;

        if (killerActor == PhotonNetwork.LocalPlayer.ActorNumber) killFeedText.text += "(YOU)";

        killFeedText.text += " killed ";
        killFeedText.text += killedName;

        if (killedActor == PhotonNetwork.LocalPlayer.ActorNumber) killFeedText.text += "(YOU)";

        yield return new WaitForSeconds(4.0f);

        killFeedText.gameObject.SetActive(false);
    }
}
