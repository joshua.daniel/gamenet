using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public AudioListener listener;
    public GameObject weapon;
    public TextMeshProUGUI playerName;
    public GameObject healthBar;

    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        this.listener = transform.Find("Camera").GetComponent<AudioListener>();

        playerName.text = photonView.Owner.NickName;
        playerName.gameObject.SetActive(!photonView.IsMine);

        camera.enabled = photonView.IsMine;
        listener.enabled = photonView.IsMine;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<VehicleAttack>().enabled = false;

            healthBar.SetActive(false);
            weapon.SetActive(false);
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = false;

            healthBar.gameObject.SetActive(true);
            weapon.SetActive(true);

            DeathRaceGameManager.instance.players.Add(this.gameObject);
        }
    }
}
