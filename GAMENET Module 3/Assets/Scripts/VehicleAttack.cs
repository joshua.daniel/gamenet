using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class VehicleAttack : MonoBehaviourPunCallbacks
{
    public int maxHealth = 1000;
    private int currentHealth;
    public int damage = 100;
    public float fireRate = 1.0f;
    public bool isLaser;

    public GameObject weapon;
    public GameObject projectilePrefab;
    public Image healthBar;
    public GameObject spectatorPrefab;

    private float fireCooldown;

    public bool isShootEnabled;

    private LineRenderer laser;

    private bool isDead = false;

    private void Start()
    {
        weapon = GetComponent<PlayerSetup>().weapon;
        laser = weapon.GetComponent<LineRenderer>();
        currentHealth = maxHealth;
        healthBar.fillAmount = (float)currentHealth / (float)maxHealth;
        isShootEnabled = false;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && isShootEnabled && photonView.IsMine)
        {
            if (!isLaser && fireCooldown <= 0)
            {
                photonView.RPC("ShootProjectile", RpcTarget.All);
            }
            else if (isLaser)
            {
                photonView.RPC("ShootLaser", RpcTarget.All);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0) && isLaser)
        {
            photonView.RPC("RemoveLaser", RpcTarget.All);
        }

        if (fireCooldown > 0) fireCooldown -= Time.deltaTime;
    }

    [PunRPC]
    public void TakeDamage(int value, int instigatorId)
    {
        currentHealth = Mathf.Max(0, currentHealth - value);
        healthBar.fillAmount = (float)currentHealth / (float)maxHealth;
        if (currentHealth <= 0) Die(instigatorId);
    }

    void Die(int killerId)
    {
        if (isDead) return;
        isDead = true;

        if (photonView.IsMine) Instantiate(spectatorPrefab, transform.Find("Camera").position, transform.Find("Camera").rotation);

        DeathRaceGameManager.instance.players.Remove(this.gameObject);

        PhotonView killer = PhotonView.Find(killerId);

        object[] data1 = new object[] { photonView.Owner.ActorNumber, photonView.Owner.NickName,
                                        killer.Owner.ActorNumber, killer.Owner.NickName };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        if (photonView.IsMine) PhotonNetwork.RaiseEvent(Constants.PlayerDeathCode, data1, raiseEventOptions, sendOption);

        if (DeathRaceGameManager.instance.players.Count <= 1)
        {
            object[] data2 = new object[] { killer.Owner.ActorNumber, killer.Owner.NickName };

            if (photonView.IsMine) PhotonNetwork.RaiseEvent(Constants.PlayerWonCode, data2, raiseEventOptions, sendOption);
        }

        Destroy(this.gameObject);
    }

    [PunRPC]
    public void ShootProjectile()
    {
        Projectile projectile = Instantiate(projectilePrefab, weapon.transform.position, weapon.transform.rotation).GetComponent<Projectile>();
        projectile.owner = this.gameObject;
        projectile.damage = damage;

        fireCooldown = 1.0f / fireRate;
    }

    [PunRPC]
    public void ShootLaser()
    {
        laser.enabled = true;

        laser.SetPosition(0, weapon.transform.position);

        RaycastHit hit;
        if (Physics.Raycast(weapon.transform.position, weapon.transform.forward, out hit, 100.0f))
        {
            laser.SetPosition(1, hit.point);
        }
        else
        {
            laser.SetPosition(1, weapon.transform.position + weapon.transform.forward * 100.0f);
        }


        if (fireCooldown <= 0 && hit.collider != null)
        {
            if (hit.collider.tag == "Player" && hit.collider.gameObject != this.gameObject)
            {
                hit.collider.gameObject.GetPhotonView().RPC("TakeDamage", RpcTarget.AllBuffered, damage, this.photonView.ViewID);

                fireCooldown = 1.0f / fireRate;
            }
        }
    }

    [PunRPC]
    public void RemoveLaser()
    {
        laser.enabled = false;
    }
}
