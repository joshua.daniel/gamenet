using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BillboardFX : MonoBehaviour
{
    public Canvas canvas;
    public Transform camTransform;

    Quaternion originalRotation;

    private Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        canvas = GetComponent<Canvas>();

        if (!transform.parent.gameObject.GetPhotonView().IsMine)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in players)
            {
                if (player.GetPhotonView().IsMine)
                {
                    camera = player.transform.Find("Camera").GetComponent<Camera>();
                }
            }

            canvas.worldCamera = camera;
            camTransform = camera.transform;
        }

        originalRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (camTransform != null)
        {
            // Change this to event
            transform.rotation = camTransform.rotation * originalRotation;
        }
    }
}
