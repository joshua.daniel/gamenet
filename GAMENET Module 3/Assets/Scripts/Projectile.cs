using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Projectile : MonoBehaviour
{
    public int damage;
    public float speed;

    public GameObject owner;

    private void Start()
    {
        Destroy(this.gameObject, 3.0f);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && other.gameObject != owner)
        {
            other.gameObject.GetPhotonView().RPC("TakeDamage", RpcTarget.AllBuffered, damage, owner.GetPhotonView().ViewID);
            Destroy(this.gameObject);
        }
    }
}
