using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpectatorMovement : MonoBehaviour
{
    private Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        camera = transform.Find("Camera").GetComponent<Camera>();

        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (var player in players)
        {
            BillboardFX ui = player.transform.Find("Canvas").GetComponent<BillboardFX>();
            ui.canvas.worldCamera = camera;
            ui.camTransform = camera.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float forward = Input.GetAxis("Vertical") * 20.0f * Time.deltaTime;
        float right = Input.GetAxis("Horizontal") * 20.0f * Time.deltaTime;

        transform.Translate(right, 0, forward);

        if (Input.GetKey(KeyCode.Mouse0))
        {
            transform.eulerAngles += 5.0f * new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
        }
    }
}
